# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BuzerList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
                ('col0', models.CharField(default=b'', max_length=32, blank=True)),
                ('col1', models.CharField(default=b'', max_length=32, blank=True)),
                ('col2', models.CharField(default=b'', max_length=32, blank=True)),
                ('col3', models.CharField(default=b'', max_length=32, blank=True)),
                ('col4', models.CharField(default=b'', max_length=32, blank=True)),
                ('col5', models.CharField(default=b'', max_length=32, blank=True)),
                ('col6', models.CharField(default=b'', max_length=32, blank=True)),
                ('col7', models.CharField(default=b'', max_length=32, blank=True)),
                ('col8', models.CharField(default=b'', max_length=32, blank=True)),
                ('col9', models.CharField(default=b'', max_length=32, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='BuzerRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('col0', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('col1', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('col2', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('col3', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('col4', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('col5', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('col6', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('col7', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('col8', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('col9', models.PositiveSmallIntegerField(default=0, choices=[(0, b' '), (1, b'+'), (2, b'-'), (3, b'0')])),
                ('list', models.ForeignKey(to='kopr.BuzerList')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='buzerrow',
            unique_together=set([('list', 'date')]),
        ),
        migrations.AlterUniqueTogether(
            name='buzerlist',
            unique_together=set([('user', 'name')]),
        ),
    ]
