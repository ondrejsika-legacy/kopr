from django.contrib import admin

from kopr.models import BuzerList, BuzerRow


class BuzerListA(admin.ModelAdmin):
    pass


class BuzerRowA(admin.ModelAdmin):
    list_display = (
        'list',
        'date',
        'col0',
        'col1',
        'col2',
        'col3',
        'col4',
        'col5',
        'col6',
        'col7',
        'col8',
        'col9',
    )


admin.site.register(BuzerList, BuzerListA)
admin.site.register(BuzerRow, BuzerRowA)
