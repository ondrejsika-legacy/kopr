from django.db import models


class BuzerList(models.Model):
    user = models.ForeignKey('auth.User')
    name = models.CharField(max_length=32)
    col0 = models.CharField(max_length=32, default='', blank=True)
    col1 = models.CharField(max_length=32, default='', blank=True)
    col2 = models.CharField(max_length=32, default='', blank=True)
    col3 = models.CharField(max_length=32, default='', blank=True)
    col4 = models.CharField(max_length=32, default='', blank=True)
    col5 = models.CharField(max_length=32, default='', blank=True)
    col6 = models.CharField(max_length=32, default='', blank=True)
    col7 = models.CharField(max_length=32, default='', blank=True)
    col8 = models.CharField(max_length=32, default='', blank=True)
    col9 = models.CharField(max_length=32, default='', blank=True)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        unique_together = ('user', 'name')


class BuzerRow(models.Model):
    CHOICES = (
        (0, ' '),
        (1, '+'),
        (2, '-'),
        (3, '0'),
    )

    list = models.ForeignKey('kopr.BuzerList')
    date = models.DateField()
    col0 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)
    col1 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)
    col2 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)
    col3 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)
    col4 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)
    col5 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)
    col6 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)
    col7 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)
    col8 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)
    col9 = models.PositiveSmallIntegerField(default=0, choices=CHOICES)

    def __unicode__(self):
        return u'%s' % self.list

    class Meta:
        unique_together = ('list', 'date')

